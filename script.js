document.onkeyup = function (e) {
	e = e || window.event;
	if (e.keyCode === 13) {
		check_function();
	}
	return false;
}

var buttons_menu = document.getElementById('buttons');
var check_button = document.createElement("input");
	check_button.type = "button";
	check_button.value = "CHECK";
	check_button.onclick = function(){check_function();}


var field = document.getElementById('field');
var plus_bool, minus_bool, mult_bool, div_bool;
var settings_loaded = false, test_loaded = false;

function set_cb(name, val, checked_){
	var chb = [];
	var checkbox = document.createElement("input");
	checkbox.id = name;
	checkbox.type = "checkbox";
	checkbox.name = name;
	checkbox.checked = checked_;
	checkbox.innerHTML = "val";

	var label = document.createElement("label");
	label.htmlFor = name;
	label.appendChild(document.createTextNode(val));

	chb[0] = checkbox;
	chb[1] = label;

	return chb;
}

function show_settings(){
	field.innerHTML = "";
	if(!settings_loaded){
		plus_bool = true;
		minus_bool = true;
		mult_bool = true;
		div_bool = true;
		settings_loaded = true;
	}

	if(test_loaded)
		buttons_menu.removeChild(check_button);

	var settings_field = document.createElement("div");
	var plus_checkbox = set_cb("plus_cb", "Plus operation", plus_bool);
	var minus_checkbox = set_cb("minus_cb", "Minus operation", minus_bool);
	var mult_checkbox = set_cb("mult_cb", "Mult operation", mult_bool);
	var div_checkbox = set_cb("div_cb", "Div operation", div_bool);

	settings_field.appendChild(plus_checkbox[0]);
	settings_field.appendChild(plus_checkbox[1]);
	settings_field.appendChild(minus_checkbox[0]);
	settings_field.appendChild(minus_checkbox[1]);
	settings_field.appendChild(mult_checkbox[0]);
	settings_field.appendChild(mult_checkbox[1]);
	settings_field.appendChild(div_checkbox[0]);
	settings_field.appendChild(div_checkbox[1]);
	field.appendChild(settings_field);
}

var results = [];

function show_test(){
	test_loaded = true;
	if(!settings_loaded){
		plus_bool = true;
		minus_bool = true;
		mult_bool = true;
		div_bool = true;
		settings_loaded = true;
	}
	else {
		plus_bool = document.getElementById('plus_cb').checked;	
		minus_bool = document.getElementById('minus_cb').checked;
		mult_bool = document.getElementById('mult_cb').checked;
		div_bool = document.getElementById('div_cb').checked;
	}


	var diff_operations = [], operations = [];
	if(plus_bool) diff_operations.push("+");
	if(minus_bool) diff_operations.push("-");
	if(mult_bool) diff_operations.push("*");
	if(div_bool) diff_operations.push("/");

	field.innerHTML = "";

	buttons_menu.appendChild(check_button);

	var nums = [];
	for(var i = 0; i < 20; i++){
		nums[i] = Math.floor((Math.random() * 10) + 1);
	}

	for(var i = 0; i < 10; i++){
		operations.push(diff_operations[Math.floor(Math.random() * diff_operations.length)]);
	}

	for(var i = 0; i < 10; i++){
		switch (operations[i]){
			case "+":
				results[i] = nums[2 * i] + nums[2 * i + 1];
				break;
			case "-":
				results[i] = nums[2 * i] - nums[2 * i + 1];
				break;
			case "*":
				results[i] = nums[2 * i] * nums[2 * i + 1];
				break;
			case "/":
				results[i] = nums[2 * i] / nums[2 * i + 1];
				break;
		}
	}

	for(var i = 0; i < 10; i++){
		var input = document.createElement("input");
		var label = document.createElement("label");
		var bool_label = document.createElement("label");
		input.type = "number";
        input.id = "input" + i;
        label.id = "label" + i;
        bool_label.id = "bool_label" + i;
        label.innerHTML = i + 1 + ") " + nums[2 * i] + " " + operations[i] + " " + nums[2 * i + 1] + " = ";
        field.appendChild(label);
        field.appendChild(input);
        field.appendChild(bool_label);
        field.appendChild(document.createElement("br"));
	}
}

function check_function(){
	for(var i = 0; i < 10; i++){
		var q = document.getElementById("input" + i).value;
		
		var bool_label = document.getElementById('bool_label' + i);
		if(q == results[i]){
			bool_label.innerHTML = "TRUE";
			bool_label.style.color = "#0F0";
		}
		else{
			bool_label.innerHTML = "FALSE";
			bool_label.style.color = "#F00";
		}
	}
}
